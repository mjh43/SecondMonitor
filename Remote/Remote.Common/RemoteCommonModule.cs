﻿namespace SecondMonitor.Remote.Common
{
    using Adapter;
    using Comparators;
    using Model;
    using Ninject.Modules;
    using ViewModels.PluginsSettings;

    public class RemoteCommonModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDatagramPayloadUnPacker>().To<DatagramPayloadUnPacker>();
            Bind<IDatagramPayloadPacker>().To<DatagramPayloadPacker>();
            Bind<ISimulatorSourceInfoComparator>().To<SimulatorSourceInfoComparator>();
            Rebind<IHostAddressValidator>().To<LiteNetLibHostAddressValidator>();
        }
    }
}