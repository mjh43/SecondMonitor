﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for CarSettingsTestControl.xaml
    /// </summary>
    public partial class CarSettingsTestControl : UserControl
    {
        public CarSettingsTestControl()
        {
            InitializeComponent();
        }
    }
}
