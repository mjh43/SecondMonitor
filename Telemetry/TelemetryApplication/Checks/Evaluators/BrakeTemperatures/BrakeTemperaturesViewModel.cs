﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.BrakeTemperatures
{
    using Result;

    public class BrakeTemperaturesViewModel : AbstractWheelEvaluationResultViewModel
    {
        public BrakeTemperaturesViewModel() : base("Brake Temps")
        {
        }
    }
}