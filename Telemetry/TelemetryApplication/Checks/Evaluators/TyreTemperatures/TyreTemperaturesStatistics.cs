﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.TyreTemperatures
{
    using System;
    using System.Linq;
    using DataModel.Snapshot.Systems;

    public class TyreTemperaturesStatistics
    {
        public TyreTemperaturesStatistics(WheelInfo wheel)
        {
            InitializeStatistics(wheel);
        }

        public QuantityStatistics LeftPortionStatistics { get; private set; }
        public QuantityStatistics CenterPortionStatistics { get; private set; }
        public QuantityStatistics RightPortionStatistics { get; private set; }
        public QuantityStatistics CorePortionStatistics { get; private set; }
        public QuantityStatistics OuterInnerDiffStatistics { get; private set; }

        private void InitializeStatistics(WheelInfo wheel)
        {
            LeftPortionStatistics = new QuantityStatistics(
                wheel.LeftTyreTemp.IdealQuantity.InCelsius -
                (wheel.LeftTyreTemp.IdealQuantityWindow.InCelsius * 1.5),
                wheel.LeftTyreTemp.IdealQuantity.InCelsius +
                (wheel.LeftTyreTemp.IdealQuantityWindow.InCelsius * 1.5),
                wheel.LeftTyreTemp.IdealQuantity.InCelsius);

            RightPortionStatistics = new QuantityStatistics(
                wheel.RightTyreTemp.IdealQuantity.InCelsius -
                (wheel.RightTyreTemp.IdealQuantityWindow.InCelsius * 1.5),
                wheel.RightTyreTemp.IdealQuantity.InCelsius +
                (wheel.RightTyreTemp.IdealQuantityWindow.InCelsius * 1.5),
                wheel.RightTyreTemp.IdealQuantity.InCelsius);

            CenterPortionStatistics = new QuantityStatistics(
                wheel.CenterTyreTemp.IdealQuantity.InCelsius -
                (wheel.CenterTyreTemp.IdealQuantityWindow.InCelsius * 1.5),
                wheel.CenterTyreTemp.IdealQuantity.InCelsius +
                (wheel.CenterTyreTemp.IdealQuantityWindow.InCelsius * 1.5),
                wheel.CenterTyreTemp.IdealQuantity.InCelsius);

            CorePortionStatistics = new QuantityStatistics(
                wheel.TyreCoreTemperature.IdealQuantity.InCelsius -
                (wheel.TyreCoreTemperature.IdealQuantityWindow.InCelsius * 1.5),
                wheel.TyreCoreTemperature.IdealQuantity.InCelsius +
                (wheel.TyreCoreTemperature.IdealQuantityWindow.InCelsius * 1.5),
                wheel.TyreCoreTemperature.IdealQuantity.InCelsius);

            OuterInnerDiffStatistics = new QuantityStatistics(0, 12, 8);
        }

        public void ProcessDataPoint(WheelInfo wheel)
        {
            LeftPortionStatistics.ProcessDataPoint(wheel.LeftTyreTemp.ActualQuantity.InCelsius);
            CenterPortionStatistics.ProcessDataPoint(wheel.CenterTyreTemp.ActualQuantity.InCelsius);
            RightPortionStatistics.ProcessDataPoint(wheel.RightTyreTemp.ActualQuantity.InCelsius);
            CorePortionStatistics.ProcessDataPoint(wheel.TyreCoreTemperature.ActualQuantity.InCelsius);

            OuterInnerDiffStatistics.ProcessDataPoint(Math.Abs(wheel.LeftTyreTemp.ActualQuantity.InCelsius - wheel.RightTyreTemp.ActualQuantity.InCelsius));
        }

        public QuantityStatistics GetHottestPart()
        {
            return new[] { LeftPortionStatistics, RightPortionStatistics, CenterPortionStatistics, CorePortionStatistics }
                .OrderByDescending(x => x.PointsOverLimit).First();
        }

        public QuantityStatistics GetColdestPart()
        {
            return new[] { LeftPortionStatistics, RightPortionStatistics, CenterPortionStatistics, CorePortionStatistics }
                .OrderBy(x => x.PointsOverLimit).First();
        }
    }
}