﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators.TyrePressures
{
    using Result;

    public class TyrePressuresViewModel : AbstractWheelEvaluationResultViewModel
    {
        public TyrePressuresViewModel() : base("Tyre Pressures")
        {
        }
    }
}