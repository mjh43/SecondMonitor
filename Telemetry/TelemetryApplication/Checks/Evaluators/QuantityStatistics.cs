﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators
{
    public class QuantityStatistics
    {
        private int _dataPointsCount;
        private double _sum;

        public QuantityStatistics(double lowLimit, double highLimit, double idealValue)
        {
            IdealValue = idealValue;
            MinValue = double.MaxValue;
            MaxValue = double.MinValue;

            HighLimit = highLimit;
            LowLimit = lowLimit;
        }

        public double IdealValue { get; }

        public int PointsOverLimit { get; private set; }

        public int PointsBelowLimit { get; private set; }

        public double MinValue { get; private set; }

        public double MaxValue { get; private set; }

        public double HighLimit { get; }

        public double LowLimit { get; }

        public double Average => _sum / _dataPointsCount;

        public void ProcessDataPoint(double value)
        {
            _dataPointsCount++;
            _sum += value;

            if (value > MaxValue)
            {
                MaxValue = value;
            }

            if (value < MinValue)
            {
                MinValue = value;
            }

            if (value > HighLimit)
            {
                PointsOverLimit++;
            }

            if (value < LowLimit)
            {
                PointsBelowLimit++;
            }
        }
    }
}