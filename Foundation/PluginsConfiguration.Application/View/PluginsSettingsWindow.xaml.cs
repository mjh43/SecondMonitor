﻿namespace SecondMonitor.PluginsConfiguration.Application.View
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for PluginsSettingsWindow.xaml
    /// </summary>
    public partial class PluginsSettingsWindow : Window
    {
        public PluginsSettingsWindow()
        {
            InitializeComponent();
        }
    }
}
