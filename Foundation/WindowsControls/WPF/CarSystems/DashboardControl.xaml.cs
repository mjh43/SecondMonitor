﻿namespace SecondMonitor.WindowsControls.WPF.CarSystems
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for DashboardControl.xaml
    /// </summary>
    public partial class DashboardControl : UserControl
    {
        public DashboardControl()
        {
            InitializeComponent();
        }
    }
}
