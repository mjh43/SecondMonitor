﻿namespace SecondMonitor.ViewModels.PitBoard
{
    public class PitReturnBoardViewModel : AbstractViewModel
    {
        private string _returnPosition;
        private string _pitTime;

        public PitReturnBoardViewModel()
        {
            ReturnPosition = "5";
            PitTime = "30.0";
        }

        public string ReturnPosition
        {
            get => _returnPosition;
            set => SetProperty(ref _returnPosition, value);
        }

        public string PitTime
        {
            get => _pitTime;
            set => SetProperty(ref _pitTime, value);
        }
    }
}