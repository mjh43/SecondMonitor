﻿namespace SecondMonitor.ViewModels.TrackRecords
{
    using System;
    using DataModel.TrackRecords;

    public class CarRecordSummaryViewModel : AbstractViewModel<NamedRecordSet>
    {
        public string CarName { get; set; }

        public TimeSpan BestOverall { get; private set; }

        public TimeSpan BestPractice { get; private set; }

        public TimeSpan BestQualification { get; private set; }

        public TimeSpan BestRace { get; private set; }

        protected override void ApplyModel(NamedRecordSet model)
        {
            CarName = model.Name;
            BestOverall = model.GetOverAllBest().LapTime;
            BestPractice = model.BestPracticeRecord?.LapTime ?? TimeSpan.Zero;
            BestQualification = model.BestQualiRecord?.LapTime ?? TimeSpan.Zero;
            BestRace = model.BestRaceRecord?.LapTime ?? TimeSpan.Zero;
        }

        public override NamedRecordSet SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}