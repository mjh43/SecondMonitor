﻿namespace SecondMonitor.ViewModels.Controllers
{
    using System;
    using System.Windows.Input;
    using Contracts.Commands;

    public class CommandFactory : ICommandFactory
    {
        public ICommand Create(Action operation)
        {
            return new RelayCommand(operation);
        }
    }
}