﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    public enum VerticalAlignment
    {
        Top,
        Bottom,
        Center
    }
}