﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    public enum VerticalAlignmentKind
    {
        Center,
        Top,
        Bottom,
        Stretch
    }
}