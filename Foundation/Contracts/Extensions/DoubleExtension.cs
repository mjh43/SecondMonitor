﻿namespace SecondMonitor.Contracts.Extensions
{
    public static class DoubleExtension
    {
        public static double ConvertRange(this double value, // value to convert
            double originalStart, double originalEnd, // original range
            double newStart, double newEnd)
        {
            double scale = (newEnd - newStart) / (originalEnd - originalStart);
            return (newStart + ((value - originalStart) * scale));
        }

        public static double ConvertRange(this float value, // value to convert
            double originalStart, double originalEnd, // original range
            double newStart, double newEnd) => ConvertRange((double)value, originalStart, originalEnd, newStart, newEnd);
    }
}