﻿namespace SecondMonitor.DataModel.BasicProperties
{
    public enum PitStopTimeDisplayKind
    {
        FullStop,
        PitStall,
        Both
    }
}