﻿namespace SecondMonitor.F12019Connector.Datamodel.Enums
{
    internal enum DriverResultKind
    {
        Na,
        Inactive,
        Active,
        Finished,
        Disqualified,
        NotClassified,
    }
}