﻿ namespace SecondMonitor.Timing.Common.SessionTiming.Drivers
{
    using DataModel.Snapshot.Drivers;
    using Lap.SectorTracker;
    using PitStatistics;
    using ViewModels.Settings;

    public class DriverTimingFactory
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ILapEventProvider _lapEventProvider;
        private readonly DriverLapSectorsTrackerFactory _driverLapSectorsTrackerFactory;
        private readonly IPitStopEvenProvider _pitStopEvenProvider;

        public DriverTimingFactory(ISettingsProvider settingsProvider, ILapEventProvider lapEventProvider, DriverLapSectorsTrackerFactory driverLapSectorsTrackerFactory, IPitStopEvenProvider pitStopEvenProvider)
        {
            _settingsProvider = settingsProvider;
            _lapEventProvider = lapEventProvider;
            _driverLapSectorsTrackerFactory = driverLapSectorsTrackerFactory;
            _pitStopEvenProvider = pitStopEvenProvider;
        }

        public DriverTiming Create(DriverInfo modelDriverInfo, ISessionInfo session, bool invalidateFirstLap)
        {
            var driver = new DriverTiming(modelDriverInfo, session, _driverLapSectorsTrackerFactory, _lapEventProvider, _settingsProvider, _pitStopEvenProvider) { InvalidateFirstLap = invalidateFirstLap };
            return driver;
        }
    }
}