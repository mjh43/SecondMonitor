﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using Common.SessionTiming.Drivers.Lap;
    using ViewModels;

    public class LapPortionTimesComparatorViewModel : AbstractViewModel, IDisposable
    {
        private readonly Stopwatch _refreshWatch;
        private TimeSpan _timeDifference;

        public LapPortionTimesComparatorViewModel()
        {
            _refreshWatch = Stopwatch.StartNew();
            TimeDifference = TimeSpan.Zero;
            SubscribeToComparedLap();
        }

        public ILapInfo ReferenceLap { get; private set; }
        public ILapInfo ComparedLap { get; private set; }

        public bool IsPaused { get; set; }

        public TimeSpan TimeDifference
        {
            get => _timeDifference;
            set => SetProperty(ref _timeDifference, value);
        }

        public void ChangeReferencedLaps(ILapInfo referencedLap, ILapInfo comparedLap)
        {
            UnSubscribeToComparedLap();
            ReferenceLap = referencedLap;
            ComparedLap = comparedLap;
            SubscribeToComparedLap();
        }

        private void SubscribeToComparedLap()
        {
            if (ComparedLap?.LapTelemetryInfo?.PortionTimes == null)
            {
                return;
            }

            TimeDifference = TimeSpan.Zero;
            ComparedLap.LapTelemetryInfo.PortionTimes.PropertyChanged += PortionTimes_PropertyChanged;
        }

        private void UnSubscribeToComparedLap()
        {
            if (ComparedLap?.LapTelemetryInfo?.PortionTimes == null)
            {
                return;
            }

            TimeDifference = TimeSpan.Zero;
            ComparedLap.LapTelemetryInfo.PortionTimes.PropertyChanged -= PortionTimes_PropertyChanged;
        }

        private void PortionTimes_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_refreshWatch.Elapsed < TimeSpan.FromMilliseconds(100) || IsPaused)
            {
                return;
            }

            if (ReferenceLap?.LapTelemetryInfo?.PortionTimes == null || ReferenceLap.LapTelemetryInfo.IsPortionTimesPurged)
            {
                TimeDifference = TimeSpan.Zero;
                return;
            }

            if (ReferenceLap.LapTelemetryInfo.PortionTimes.GetTimeAtDistance(ComparedLap.CompletedDistance) != TimeSpan.Zero)
            {
                TimeDifference = ComparedLap.LapTelemetryInfo.PortionTimes.GetTimeAtDistance(ComparedLap.CompletedDistance)
                                 - ReferenceLap.LapTelemetryInfo.PortionTimes.GetTimeAtDistance(ComparedLap.CompletedDistance);
            }
            else
            {
                TimeDifference = TimeSpan.Zero;
            }

            _refreshWatch.Restart();
        }

        public void Dispose()
        {
            UnSubscribeToComparedLap();
        }
    }
}